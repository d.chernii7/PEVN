const ApiError = require('../error/apiError')
const bcrypt = require('bcrypt')
const jwt = require('jsonwebtoken')
const {User, Basket} = require('../models/models')

const generateJwt = (id, email, role) => {
    return jwt.sign({id, email, role}, process.env.SECRET_KEY, {expiresIn: '15m'})
}

class UserController {
    async registration(req, res) {
        const {email, password, role} = req.body

        if (!email || !password) {
            return next(ApiError.badRequest('email or password is incorrect '))
        }

        const candidate = await User.findOne({where: {email}})

        if (candidate) {
            return next(ApiError.badRequest('current email has been registered'))
        }

        const hashPassword = await bcrypt.hash(password, 5)
        const user = await User.create({email, role, password: hashPassword})
        const basket = await Basket.create({userId: user.id})
        const token = generateJwt(user.id, user.email, user.role)

        return res.json({token})
    }

    async login(req, res, next) {
        const {email, password} = req.body
        const user = await User.findOne({where: {email}})

        if (!user) {
            return next(ApiError.badRequest('email was not registered'))
        }

        let comparePassword = bcrypt.compareSync(password, user.password)

        if (!comparePassword) {
            return next(ApiError.badRequest('password is incorrect'))
        }

        const token = generateJwt(user.id, user.email, user.role)
        return res.json({token})

    }

    async check(req, res, next) {
        res.json({ message: "SdSDSDS"})
    }
}

module.exports = new UserController()
